package machine;

public class TunnelStatistics {

    private int howMuchCars;
    private int howMuchTrains;
    private int howMuchTirs;

    public TunnelStatistics(int howMuchCars, int howMuchTrains, int howMuchTirs) {
        this.howMuchCars = howMuchCars;
        this.howMuchTrains = howMuchTrains;
        this.howMuchTirs = howMuchTirs;
    }

    public int getHowMuchCars() {
        return howMuchCars;
    }

    public int getHowMuchTrains() {
        return howMuchTrains;
    }

    public int getHowMuchTirs() {
        return howMuchTirs;
    }


}
