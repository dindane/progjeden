package machine;

import java.util.ArrayList;
import java.util.List;

public class Tunel {

    private List<Pojazd> vehiclesList = new ArrayList<>();

    public void howMuchVehicles(){
        System.out.println("Ilosc pojazdow w tunelu wynosi: " + vehiclesList.size());
    }

    public void addVehicles(Pojazd pojazd){
        vehiclesList.add(pojazd);
    }

    public int averageVehiclesSpeed(){
        int sumVehiclesSpeed = 0;
        for(int i = 0; i < vehiclesList.size(); i++){
            sumVehiclesSpeed += vehiclesList.get(i).getPredkosc();
        }
        return (sumVehiclesSpeed/vehiclesList.size());
    }

    public Pojazd vehicleWithMinSpeed(){
        int minSpeed = vehiclesList.get(0).getPredkosc();
        int indexVehicleWithMinSpeed = 0;
        for(int i = 1; i < vehiclesList.size(); i++){
            if(minSpeed > vehiclesList.get(i).getPredkosc()){
                minSpeed = vehiclesList.get(i).getPredkosc();
                indexVehicleWithMinSpeed = i;
            }
        }
        return vehiclesList.get(indexVehicleWithMinSpeed);
    }

    public Pojazd vehicleWithMaximumSpeed(){
        int maxSpeed = vehiclesList.get(0).getPredkosc();
        int indexVehicleWithMaxSpeed = 0;
        for(int i = 1; i < vehiclesList.size(); i++){
            if(maxSpeed < vehiclesList.get(i).getPredkosc()){
                maxSpeed = vehiclesList.get(i).getPredkosc();
                indexVehicleWithMaxSpeed = i;
            }
        }
        return vehiclesList.get(indexVehicleWithMaxSpeed);
    }

    public TunnelStatistics makeTunnelStatistics(){
        int cars = 0;
        int trains = 0;
        int tirs = 0;
        for(Pojazd p : vehiclesList){
            if(p instanceof Samochod){
                cars++;
            }
            if(p instanceof Pociag){
                trains++;
            }
            if(p instanceof Tir){
                tirs++;
            }
        }
        return new TunnelStatistics(cars,trains,tirs);
    }

    public int howManyVehiclesBack(){
        int vehiclesBack = 0;
        for(int i = 0; i < vehiclesList.size(); i++){
            if(vehiclesList.get(i).getPredkosc() < 0){
                vehiclesBack++;
            }
        }
        return vehiclesBack;
    }

    public int howManyVehiclesHaveOverSpeed(){
        int vehiclesHavingOverSpeed = 0;
        for(int i = 0; i < vehiclesList.size(); i++){
            if(vehiclesList.get(i).getPredkosc() > 80){
                vehiclesHavingOverSpeed++;
            }
        }
        return vehiclesHavingOverSpeed;
    }

    public int howManyDeletedCarHavingOverSpeed(){
        int deletedCar = 0;
        for(int i =0; i < vehiclesList.size(); i++){
            if(vehiclesList.get(i).getPredkosc() > 80){
                vehiclesList.remove(i);
                deletedCar++;
            }
        }
        return deletedCar;
    }

}
