package machine;

public class Samochod extends Pojazd {

    public String getModel() {
        return model;
    }

    private String model;

    public Samochod(String model) {
        this.model = model;
    }

    public void jedz() {
        System.out.println("Samochód jedzie z prędkością " + getPredkosc());
    }

    public void stoj() {
        setPredkosc(0);
        System.out.println("Samochód się zatrzymał.");
    }

    public void przyspiesz() {
        if (getPredkosc() <= 218) {
            setPredkosc(getPredkosc() + 2);
            System.out.println("Samochód przyspieszył do prędkości " + getPredkosc());
        } else if (getPredkosc() > 100 && getPredkosc() <= 220 - 2 - 2 * aLetters(this.model)) {
            setPredkosc(getPredkosc() + 2 + 2 * aLetters(this.model));

        } else {
            System.out.println("Samochód jedzie z prędkością maksymalną - niemożliwe przyspieszenie.");
        }
    }

    public void zwolnij() {
        if (getPredkosc() >= -40) {
            setPredkosc(getPredkosc() - 5);
            System.out.println("Samochód zwolnił do prędkości " + getPredkosc());
        } else {
            System.out.println("Samochód cofa z prędkością maksymalną.");
        }
    }

    public void trab() {
        System.out.println("Samochód trąbi.");

    }

    public int aLetters(String model) {
        this.model = model;
        int count = 0;
        for (int i = 0; i < model.length(); i++) {
            if (model.charAt(i) == 'a' || model.charAt(i) == 'A') {
                count++;
            }
        }
        return count;
    }
}
