package machine;

public abstract class Pojazd {
    private int predkosc;
    public abstract void jedz();
    public abstract void stoj();
    public abstract void przyspiesz();
    public abstract void zwolnij();
    public abstract void trab();

    public int getPredkosc() {
        return predkosc;
    }

    public void setPredkosc(int predkosc) {
        this.predkosc = predkosc;
    }
}
