package machine;

import static machine.RodzajPociagu.OSOBOWY;

public class Pociag extends Pojazd {

    private RodzajPociagu rodzajPociagu;

    public Pociag(RodzajPociagu rodzajPociagu) {
        this.rodzajPociagu = rodzajPociagu;
    }

    @Override
    public void jedz() {
        System.out.println("Pociąg jedzie z prędkością " + getPredkosc());
    }

    @Override
    public void stoj() {
        setPredkosc(0);
        System.out.println("Pociąg się zatrzymał");
    }

    @Override
    public void przyspiesz() {
        if(getPredkosc() <= 100){
            setPredkosc(getPredkosc() + 5);
            System.out.println("Pociag przyspieszyl do prekosci " + getPredkosc());
        }
    }

    @Override
    public void zwolnij() {
        if(rodzajPociagu == OSOBOWY){
            if(getPredkosc() >= -10){
                setPredkosc(getPredkosc() - 7);
                System.out.println("Pociag osobowy zwolnil do predkosci " + getPredkosc());
            } else {
                System.out.println("Pociag osobowy cofa z predkoscia maksymalna 10km/h");
            }
        } else {
            if(getPredkosc() > 0){
                setPredkosc(getPredkosc() - 7);
                System.out.println("Pociąg towarowy zwolnil do predkosci " + getPredkosc());
            } else {
                System.out.println("Pociag towarowy nie moze cofac");
            }
        }
    }

    @Override
    public void trab() {
        if(getPredkosc() > 0){
            setPredkosc((int)(getPredkosc()*0.9));
        }
        System.out.println("Pociag " + rodzajPociagu + " zatrabil i zwolnil do predkosci " + getPredkosc());
    }
}
