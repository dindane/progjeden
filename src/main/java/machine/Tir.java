package machine;

public class Tir extends Pojazd {
    private String marka;
    private static int trabCounter = 0;

    public void jedz() {
        System.out.println("Samochód jedzie z prędkością " + getPredkosc());
    }

    public void stoj() {
        setPredkosc(0);
        System.out.println("Tir się zatrzymał.");
    }

    public void przyspiesz() {
        if (getPredkosc() <= 79) {
            setPredkosc(getPredkosc() + 1);
            System.out.println("Tir przyspieszył do prędkości " + getPredkosc());
        }
    }

    public void zwolnij() {
        if (getPredkosc() >= -10) {
            setPredkosc(getPredkosc() - 3 - min(3 * trabCounter, 7));
            System.out.println("Tir zwolnił do prędkości " + getPredkosc());
        } else {
            System.out.println("Tir cofa z prędkością maksymalną.");
        }
    }

    public void trab() {
        trabCounter++;

        System.out.println("Tir trąbi.");

    }

    public int min(int a, int b) {
        return (a < b) ? a : b;
    }
}
